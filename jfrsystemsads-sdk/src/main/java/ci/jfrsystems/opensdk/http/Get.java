package ci.jfrsystems.opensdk.http;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.os.StrictMode.ThreadPolicy.Builder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import ci.jfrsystems.opensdk.AdsSDK;

public class Get {
    public Get() {
    }

    public static String streamToString(InputStream is1) {
        BufferedReader rd = new BufferedReader(new InputStreamReader(is1), 4096);
        StringBuilder sb = new StringBuilder();

        try {
            String line;
            while((line = rd.readLine()) != null) {
                sb.append(line);
            }

            rd.close();
        } catch (IOException var5) {
            var5.printStackTrace();
        }

        return   sb.toString();
    }

    public static InputStream getUrl2Stream(String _url) {
        InputStream result = null;

        try {
            ThreadPolicy e = (new Builder()).permitAll().build();
            StrictMode.setThreadPolicy(e);
            URL url = new URL(URLEncoder.encode(_url, "utf-8"));

            try {
                result = url.openStream();
            } catch (IOException var5) {
                var5.printStackTrace();
            }
        } catch (MalformedURLException | UnsupportedEncodingException var6) {
            var6.printStackTrace();
        }

        return result;
    }

    public static String getHtml(String url) {
        ThreadPolicy policy = (new Builder()).permitAll().build();
        StrictMode.setThreadPolicy(policy);
        InputStream in = getUrl2Stream(url);
        return streamToString(in);
    }

    public static void redirectTo(Context mContext, String pRedirect) {
        if(mContext != null && pRedirect.contains("//")) {
            if(!AdsSDK.getIDApplication().isEmpty()) {
                pRedirect = pRedirect.replace("%%ADVERTISING_APPLICATION_ID%%", AdsSDK.getIDApplication());

            }

            Intent i = new Intent("android.intent.action.VIEW", Uri.parse(pRedirect));
            i.addFlags(268435456);
            mContext.startActivity(i);
        }

    }
}