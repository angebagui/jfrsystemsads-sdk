package ci.jfrsystems.opensdk.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import ci.jfrsystems.opensdk.http.Get;
import ci.jfrsystems.opensdk.listeners.AdsCallback;
import ci.jfrsystems.opensdk.views.AdsImageView.OnImageChangeListener;
import ci.jfrsystems.opensdk.views.AdsVideoView.PlayPauseListener;

public class Constructor {
    public Constructor() {
    }

    public static AdsImageView createImageView(final Context _context, int _resource, final String _redirect, final boolean _show, final AdsCallback callback) {
        AdsImageView finalView = null;
        if(_context != null) {
            finalView = new AdsImageView(_context);
            Drawable d = _context.getResources().getDrawable(_resource);
            finalView.setImageChangeListener(new OnImageChangeListener() {
                public void imageChangedInView() {
                    if(_show && callback != null) {
                        callback.onShow();
                    }

                }
            });
            finalView.setVisibility(View.INVISIBLE);
            if(_redirect != null && !_redirect.isEmpty()) {
                finalView.setOnClickListener(new OnClickListener() {
                    public void onClick(View arg0) {
                        Get.redirectTo(_context, _redirect);
                        if(callback != null) {
                            callback.onClick();
                        }

                    }
                });
            }

            if(d != null) {
                finalView.setImageDrawable(d);
            }
        }

        return finalView;
    }

    public static AdsWebView createImageView(final Context _context, String _url, String _redirect, final boolean _show, int width, int height, final AdsCallback callback) {
        AdsWebView finalView = null;
        if(_context != null) {
            finalView = new AdsWebView(_context);
            finalView.setVisibility(View.INVISIBLE);
            finalView.getSettings().setJavaScriptEnabled(true);
            String w = "";
            String h = "";
            w = width > 1?width + "px":"100%";
            h = height > 1?height + "px":"100%";
            String contentHTML = "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\"><style>body,html{margin:0; padding:0;}img{width:" + w + "; height:" + h + ";}</style></head><body style=\"margin:auto; text-align:center; padding:0; vertical-align:middle;\"><a href=\"" + _redirect + "\"><img src=\"" + _url + "\" /></a></body></html>";
            finalView.loadData(contentHTML, "text/html", "utf-8");
            final String url_origin = finalView.getUrl();
            finalView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if(url != null && url != url_origin) {
                        Get.redirectTo(_context, url);
                        if(callback != null) {
                            callback.onClick();
                        }

                        return true;
                    } else {
                        return false;
                    }
                }

                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    if(_show && callback != null) {
                        callback.onShow();
                    }

                }

                public void onLoadResource(WebView view, String url) {
                }
            });
        }

        return finalView;
    }

    public static AdsWebView createWebView(final Context _context, String _contentHTML, String bannerBGColor, final boolean _show, final AdsCallback callback) {
        AdsWebView finalView = null;
        if(_context != null) {
            String contentHTML = "";
            if(!_contentHTML.contains("<")) {
                contentHTML = Get.getHtml(_contentHTML);
            } else {
                contentHTML = "<html><head></head><body style=\"margin:auto; text-align:center; padding:0; vertical-align:middle; background-color:" + bannerBGColor + ";\">" + _contentHTML + "</body></html>";
            }

            finalView = new AdsWebView(_context);
            finalView.setVisibility(View.INVISIBLE);
            finalView.getSettings().setJavaScriptEnabled(true);
            finalView.loadData(contentHTML, "text/html", "utf-8");
            finalView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {

                        Get.redirectTo(_context, url);
                        if(callback != null) {
                            callback.onClick();
                        }

                        return true;

                }

                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    if(_show && callback != null) {
                        callback.onShow();
                    }

                }

                public void onLoadResource(WebView view, String url) {
                }
            });
        }

        return finalView;
    }

    public static AdsVideoView createVideoView(final Context _context, String _url, final String _redirect, final boolean _show, final AdsCallback callback) {
        AdsVideoView finalView = null;
        if(_context != null) {
            finalView = new AdsVideoView(_context);
            finalView.setVisibility(View.INVISIBLE);
            finalView.setOnTouchListener(new OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent arg1) {
                    Get.redirectTo(_context, _redirect);
                    if(callback != null) {
                        callback.onClick();
                    }

                    return false;
                }
            });
            finalView.setPlayPauseListener(new PlayPauseListener() {
                public void onPlay() {
                    if(_show && callback != null) {
                        callback.onShow();
                    }

                }

                public void onPause() {
                }
            });
            finalView.setVideoPath(_url);
            finalView.start();
        }

        return finalView;
    }
}
