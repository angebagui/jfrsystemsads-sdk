package ci.jfrsystems.opensdk.views;

import android.content.Context;
import android.widget.VideoView;

public class AdsVideoView extends VideoView {
    private AdsVideoView.PlayPauseListener mListener;

    public AdsVideoView(Context context) {
        super(context);
    }

    @Override
    public void pause() {
        super.pause();
        if(this.mListener != null) {
            this.mListener.onPause();
        }

    }

    @Override
    public void start() {
        super.start();
        if(this.mListener != null) {
            this.mListener.onPlay();
        }

    }

    public void setPlayPauseListener(AdsVideoView.PlayPauseListener listener) {
        this.mListener = listener;
    }

    public interface PlayPauseListener {
        void onPlay();

        void onPause();
    }
}
