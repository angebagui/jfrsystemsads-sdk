package ci.jfrsystems.opensdk.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;

public class AdsImageView extends AppCompatImageView {
    private AdsImageView.OnImageChangeListener onImageChangeListener;

    public AdsImageView(Context context) {
        super(context);
    }

    public void setImageChangeListener(AdsImageView.OnImageChangeListener onImageChangeListener) {
        this.onImageChangeListener = onImageChangeListener;
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        if(this.onImageChangeListener != null) {
            this.onImageChangeListener.imageChangedInView();
        }

    }

    public void setImageDrawable(Drawable bitmap) {
        super.setImageDrawable(bitmap);
        if(this.onImageChangeListener != null) {
            this.onImageChangeListener.imageChangedInView();
        }

    }

    public interface OnImageChangeListener {
        void imageChangedInView();
    }
}
