package ci.jfrsystems.opensdk.views;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

import ci.jfrsystems.opensdk.JSON;
import ci.jfrsystems.opensdk.listeners.AdsCallback;
import ci.jfrsystems.opensdk.tools.Globals;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class AdsActivity extends Activity {
    private RelativeLayout rl;
    private Button buttonClose;
    private View finalView;
    private DisplayMetrics metrics;
    private float scaledDensity;
    private int buttonCloseWidth = 40;
    private int buttonCloseHeight = 40;
    private String buttonCloseUrl = "";
    private String contentType = "";
    private String contentHTML = "";
    private String bannerBGColor = "";
    private String redirect = "";
    private int buttonTimeToOnset = 0;
    private int timeout_close = 0;
    private Animation animation;

    int timeout_close_tmp = AdsActivity.this.timeout_close;

    private AdsCallback adsCallback = new AdsCallback() {
        public void onShow() {
        }

        public void onClick() {
            AdsActivity.this.finish();
        }
    };
    private int autopromoInterstitial = 0;
    private int autopromoBanner = 0;
    private String linkAutopromo = "";
    private boolean forceAutopromo = false;
    private boolean showTimeout = true;

    public AdsActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutParams params = null;
        this.metrics = this.getResources().getDisplayMetrics();
        this.scaledDensity = this.metrics.scaledDensity;
        Intent i = this.getIntent();

        try {
            this.buttonCloseUrl = i.getStringExtra("buttonCloseUrl");
            this.buttonTimeToOnset = i.getIntExtra("buttonTimeToOnset", 1);
            this.timeout_close = i.getIntExtra("timeout_close", 0);
            this.contentType = i.getStringExtra("contentType");
            this.contentHTML = i.getStringExtra("contentHTML");
            this.bannerBGColor = i.getStringExtra("bannerBGColor");
            this.redirect = i.getStringExtra("redirect");
            this.forceAutopromo = i.getBooleanExtra("forceAutopromo", false);
            this.linkAutopromo = i.getStringExtra("linkAutopromo");
            this.autopromoInterstitial = i.getIntExtra("autopromoInterstitial", 0);
        } catch (Exception var6) {
            System.out.println(var6);
        }

        this.rl = new RelativeLayout(this);
        this.rl.setBackgroundColor(0xff000000);
        LayoutParams lp = new LayoutParams(-1, -1);
        lp.leftMargin = 0;
        lp.rightMargin = 0;
        lp.topMargin = 0;
        lp.bottomMargin = 0;
        lp.addRule(13);
        this.rl.setLayoutParams(lp);
        if(this.forceAutopromo) {
            if(this.autopromoInterstitial > 0) {
                this.finalView = Constructor.createImageView(this.getApplicationContext(), this.autopromoInterstitial, this.linkAutopromo, false, this.adsCallback);
            }
        } else if(this.contentType.toUpperCase().equals(JSON.ContentType.HTML)) {
            this.finalView = Constructor.createWebView(this.getApplicationContext(), this.contentHTML, this.bannerBGColor, false, this.adsCallback);
        } else if(this.contentType.toUpperCase().equals(JSON.ContentType.IMAGE)) {
            if(Globals.isGif(this.contentHTML)) {
                this.finalView = Constructor.createImageView(this.getApplicationContext(), this.contentHTML, this.redirect, false, 0, 0, this.adsCallback);
            } else {
                this.finalView = Constructor.createImageView(this.getApplicationContext(), this.contentHTML, this.redirect, false, 0, 0, this.adsCallback);
            }
        } else if(this.contentType.toUpperCase().equals(JSON.ContentType.VIDEO)) {
            this.finalView = Constructor.createVideoView(this.getApplicationContext(), this.contentHTML, this.redirect, false, this.adsCallback);
        }

        if(this.metrics != null) {
            params = new LayoutParams(this.metrics.widthPixels, this.metrics.heightPixels);
            params.width = this.metrics.widthPixels;
            params.height = this.metrics.heightPixels;
        } else {
            params = new LayoutParams(-1, -1);
        }

        params.leftMargin = 0;
        params.rightMargin = 0;
        params.topMargin = 0;
        params.bottomMargin = 0;
        params.addRule(13);
        this.finalView.setLayoutParams(params);
        this.finalView.requestFocus();
        this.rl.addView(this.finalView);
        this.finalView.setVisibility(View.VISIBLE);
        this.buttonClose = new Button(this);
        this.buttonClose.setBackgroundColor(0xff000000);
        this.buttonClose.setText("");
        this.buttonClose.setTextColor(-1);
        this.buttonClose.setBackgroundResource(android.R.drawable.ic_menu_close_clear_cancel);
        (new Thread(new Runnable() {
            public void run() {
                if(AdsActivity.this.buttonCloseUrl != null && !AdsActivity.this.buttonCloseUrl.equals("")) {
                    try {
                        InputStream is = (InputStream)(new URL(AdsActivity.this.buttonCloseUrl)).getContent();
                        final Drawable buttonBg = Drawable.createFromStream(is, (String)null);
                        AdsActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                AdsActivity.this.buttonClose.setText(" ");
                                if(buttonBg != null) {
                                    AdsActivity.this.buttonClose.setBackgroundDrawable(buttonBg);
                                }

                            }
                        });
                    } catch (MalformedURLException var3) {
                        ;
                    } catch (IOException var4) {
                        ;
                    }
                }

            }
        })).start();
        LayoutParams paramsButton = new LayoutParams((int)((float)this.buttonCloseWidth * this.scaledDensity), (int)((float)this.buttonCloseHeight * this.scaledDensity));
        paramsButton.addRule(11);
        paramsButton.addRule(10);
        paramsButton.setMargins(0, 15 + Globals.getStatusBarHeight(this), 45, 0);
        this.buttonClose.setLayoutParams(paramsButton);
        this.buttonClose.getLayoutParams().height = paramsButton.height;
        this.buttonClose.getLayoutParams().width = paramsButton.width;
        this.buttonClose.setVisibility(View.INVISIBLE);
        this.buttonClose.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                AdsActivity.this.finish();
            }
        });
        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                AdsActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        AdsActivity.this.buttonClose.setVisibility(View.VISIBLE);
                    }
                });
            }
        }, (long)this.buttonTimeToOnset);
        this.rl.addView(this.buttonClose);
        this.setContentView(this.rl);
    }

    protected void onStart() {
        super.onStart();
        if(this.timeout_close > 0) {
            if(this.showTimeout) {
                final TextView time = new TextView(this);
                time.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        AdsActivity.this.rl.removeView(v);
                    }
                });
                this.runOnUiThread(new Runnable() {
                    public void run() {
                        time.setBackgroundColor(0xff000000);
                        time.setTextColor(-1);
                        LayoutParams paramsTime = new LayoutParams(-1, (int)((float)AdsActivity.this.buttonCloseHeight * AdsActivity.this.scaledDensity));
                        paramsTime.addRule(12);
                        paramsTime.addRule(9);
                        paramsTime.setMargins(15, 0, 15, 90);
                        time.setPadding(10, 7, 10, 7);
                        time.getBackground().setAlpha(128);
                        time.setLayoutParams(paramsTime);
                        time.setGravity(17);
                        AdsActivity.this.rl.addView(time);
                    }
                });
                (new Thread(new Runnable() {
                    public void run() {

                        while(timeout_close_tmp > 0) {
                            AdsActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    time.setText("Fermeture automatique dans : " + timeout_close_tmp / 1000 + "s");
                                }
                            });

                            try {
                                Thread.sleep(1000L);
                                timeout_close_tmp -= 1000;
                            } catch (Exception var4) {
                                ;
                            }
                        }

                    }
                })).start();
            }

            (new Handler()).postDelayed(new Runnable() {
                public void run() {
                    if(this != null) {
                        AdsActivity.this.finish();
                    }

                }
            }, (long)this.timeout_close);
        }

    }

    protected void onRestart() {
        super.onRestart();
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
    }

    protected void onDestroy() {
        if(this.metrics != null) {
            this.metrics = null;
        }

        super.onDestroy();
    }
}
