package ci.jfrsystems.opensdk.tools;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import java.lang.reflect.Field;

public class Animations {
    public static final int FADE_IN = 1;
    public static final int FADE_OUT = 2;
    public static final int SLIDE_DOWN = 3;
    public static final int SLIDE_UP = 4;
    public static final int SLIDE_LEFT = 5;
    public static final int SLIDE_RIGHT = 6;
    public static final int ROTATION_LEFT = 7;
    public static final int ROTATION_RIGHT = 8;
    public static final int FLIP_HORIZONTAL = 9;
    public static final int FLIP_VERTICAL = 10;
    public static final int FAST = 500;
    public static final int MEDIUM = 1000;
    public static final int SLOW = 3000;

    public Animations() {
    }

    public static int getIDAnimation(String libelle) {
        Field field = null;
        int retour = 0;

        try {
            field = Animations.class.getDeclaredField(libelle);
        } catch (NoSuchFieldException var5) {
            var5.printStackTrace();
        }

        if(!field.equals((Object)null)) {
            try {
                retour = field.getInt((Object)null);
            } catch (IllegalAccessException var4) {
                var4.printStackTrace();
            }
        }

        return retour;
    }

    public static int getAnimationSpeed(String libelle) {
        return libelle.equals("FAST")?FAST:(libelle.equals("MEDIUM")?MEDIUM:(libelle.equals("SLOW")?SLOW:0));
    }



    public static Animation Animate(int _effect, int _speed) {
        Object effect = null;
        switch(_effect) {
        case FADE_IN:
            effect = new AlphaAnimation(0.0F, 1.0F);
            ((Animation)effect).setDuration((long)_speed);
            ((Animation)effect).setStartOffset(0L);
            break;
        case FADE_OUT:
            effect = new AlphaAnimation(1.0F, 0.0F);
            ((Animation)effect).setDuration((long)_speed);
            ((Animation)effect).setStartOffset(0L);
            break;
        case SLIDE_DOWN:
            effect = new TranslateAnimation(2, 0.0F, 2, 0.0F, 2, -1.0F, 2, 0.0F);
            ((Animation)effect).setDuration((long)_speed);
            ((Animation)effect).setStartOffset(0L);
            break;
        case SLIDE_UP:
            effect = new TranslateAnimation(2, 0.0F, 2, 0.0F, 2, 1.0F, 2, 0.0F);
            ((Animation)effect).setDuration((long)_speed);
            ((Animation)effect).setStartOffset(0L);
            break;
        case SLIDE_LEFT:
            effect = new TranslateAnimation(2, 1.0F, 2, 0.0F, 2, 0.0F, 2, 0.0F);
            ((Animation)effect).setDuration((long)_speed);
            ((Animation)effect).setStartOffset(0L);
            break;
        case SLIDE_RIGHT:
            effect = new TranslateAnimation(2, -1.0F, 2, 0.0F, 2, 0.0F, 2, 0.0F);
            ((Animation)effect).setDuration((long)_speed);
            ((Animation)effect).setStartOffset(0L);
        case ROTATION_LEFT:
        case ROTATION_RIGHT:
        default:
            break;
        case FLIP_HORIZONTAL:
            effect = new ScaleAnimation(1.0F, 1.0F, 0.5F, 2.0F, 1, 0.5F, 1, 0.5F);
            ((Animation)effect).setDuration((long)(_speed / 2));
            ((Animation)effect).setStartOffset(0L);
            break;
        case FLIP_VERTICAL:
            effect = new ScaleAnimation(-1.0F, 1.0F, 0.0F, 1.0F, 1, 0.5F, 1, 0.5F);
            ((Animation)effect).setDuration((long)_speed);
            ((Animation)effect).setStartOffset(0L);
        }

        return (Animation)effect;
    }
}
