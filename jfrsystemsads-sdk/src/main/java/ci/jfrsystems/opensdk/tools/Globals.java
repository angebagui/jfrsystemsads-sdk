package ci.jfrsystems.opensdk.tools;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;

import ci.jfrsystems.opensdk.AdsSDK;
import ci.jfrsystems.opensdk.Slot;
import ci.jfrsystems.opensdk.http.Get;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Globals {
    public Globals() {
    }

    public static boolean isConnectedNetwork(Context context) {
        if(context == null) {
            return false;
        } else {
            ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
        }
    }

    public static int getStatusBarHeight(Activity _activity) {
        int result = 0;
        int resourceId = _activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if(resourceId > 0) {
            result = _activity.getResources().getDimensionPixelSize(resourceId);
        }

        return result;
    }

    public static String getMacAddress(Activity _activity) {
        if(_activity.getPackageManager().checkPermission("android.permission.ACCESS_WIFI_STATE", _activity.getPackageName()) == 0) {
            WifiManager manager = (WifiManager)_activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = manager.getConnectionInfo();
            return info.getMacAddress();
        } else {
            return "";
        }
    }

    public static String getStateNetwork(Context context) {
        if(context != null) {
            if(!isConnectedNetwork(context)) {
                return "no_network";
            } else {
                ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
                if(cm.getNetworkInfo(1).isConnectedOrConnecting()) {
                    return "WIFI";
                } else {
                    int netSubType = cm.getActiveNetworkInfo().getSubtype();
                    switch(netSubType) {
                    case 1:
                    case 2:
                    case 4:
                    case 7:
                    case 11:
                        return "GPRS";
                    case 3:
                    case 5:
                    case 6:
                    case 8:
                    case 9:
                    case 10:
                    case 12:
                    case 14:
                    case 15:
                        return "3G";
                    case 13:
                        return "4G";
                    default:
                        return "Unknown";
                    }
                }
            }
        } else {
            return "";
        }
    }

    /**
     * A 64-bit number (as a hex string) that is randomly generated w
     * hen the user first sets up the device and should remain constant
     * for the lifetime of the user's device.
     * The value may change if a factory reset is performed on the device.
     * @param context
     * @return
     */
    public static String getDeviceId(Context context) {
        try {
            if(!Secure.getString(context.getContentResolver(), Secure.ANDROID_ID).equals("")) {
                return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
            }
        } catch (Exception var2) {
            ;
        }

        return "";
    }


    public static String getMd5Hash(String input) {
        try {
            MessageDigest e = MessageDigest.getInstance("MD5");
            byte[] messageDigest = e.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);

            String md5;
            for(md5 = number.toString(16); md5.length() < 32; md5 = "0" + md5) {
                ;
            }

            return md5;
        } catch (NoSuchAlgorithmException var5) {
            Log.e("MD5", var5.getLocalizedMessage());
            return null;
        }
    }

    public static boolean isGif(String url) {
        return url.toLowerCase().contains(".gif");
    }

    public static Bitmap getBitmap(String url) {
        Bitmap bmp = null;

        try {
            bmp = BitmapFactory.decodeStream(Get.getUrl2Stream(url));
        } catch (Exception var3) {
            Log.e("ERROR", var3.getMessage());
        }

        return bmp;
    }

    public static String getOSVersion() {
        String building = "unknown";

        try {
            building = VERSION.RELEASE;
        } catch (Exception var2) {
            ;
        }

        return building;
    }

    public static String getDeviceName() {
        String manufacturer = "unknown";
        String model = "unknown";

        try {
            manufacturer = Build.MANUFACTURER.replaceAll(" ", "-");
            model = Build.MODEL.replaceAll(" ", "-");
            return model.startsWith(manufacturer)?model:manufacturer + "-" + model;
        } catch (Exception var3) {
            return manufacturer;
        }
    }

    public static void trackEvent(final Context context, final long idbanner, final int event, final Location location) {
        if(idbanner > 0 && !TextUtils.isEmpty(AdsSDK.getIDApplication())) {

            Observable.create(new ObservableOnSubscribe<String>() {
                @Override
                public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                    try {
                        long timestamp = System.currentTimeMillis();
                        String response =  Get.getHtml(AdsSDK.API_TRACK_EVENT_URL+"idbanner=" + idbanner
                                + "&idapp=" + AdsSDK.getIDApplication()
                                +"&event="+event
                                + "&ms=" + timestamp
                                + "&isMobile=1"
                                + "&geocoord=" + location.getLatitude() + ";" + location.getLongitude()
                                +"&isTesting="+(AdsSDK.testing?"1":"0")
                                +"&network=" +Globals.getStateNetwork(context)
                                +"&deviceId=" + Globals.getDeviceId(context)
                                +"&devicesOSVersion=Android " + Globals.getOSVersion()
                                +"&devicesName=" + Globals.getDeviceName()
                        );
                        e.onNext(response);

                    }catch (Exception exc){
                        e.onError(exc);
                    }finally {
                        e.onComplete();
                    }

                }
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<String>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {

                        }

                        @Override
                        public void onNext(@NonNull String s) {
                            System.out.println(s);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            System.out.println("Error found "+e);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }

    }

  public static String updateMacro(String s) {
        if(!AdsSDK.getIDApplication().isEmpty()) {
            s = s.replace("%%ADVERTISING_APPLICATION_ID%%", AdsSDK.getIDApplication());
        }

        return s;
    }


}
