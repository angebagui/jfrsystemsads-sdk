package ci.jfrsystems.opensdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Build.VERSION;
import android.os.StrictMode.ThreadPolicy;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.LinearLayout;

import ci.jfrsystems.opensdk.R.styleable;
import ci.jfrsystems.opensdk.formats.Format;
import ci.jfrsystems.opensdk.formats.NativeCustomConfiguration;
import ci.jfrsystems.opensdk.http.Get;
import ci.jfrsystems.opensdk.listeners.AdsCallback;
import ci.jfrsystems.opensdk.listeners.SlotListener;
import ci.jfrsystems.opensdk.tools.Animations;
import ci.jfrsystems.opensdk.tools.Globals;
import ci.jfrsystems.opensdk.views.AdsActivity;

import ci.jfrsystems.opensdk.views.Constructor;
import static ci.jfrsystems.opensdk.JSON.Key.*;


import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class Slot extends LinearLayout implements SlotListener, Serializable {

    // CONSTANT
    public static final int DEFAULT_BUTTON_CLOSE_SIZE = 35;
    public static final int DEFAULT_BUTTON_CLOSE_TIME = 2000;
    public static final String DEFAULT_BANNER_BACKGROUND_COLOR = "#000";
    public static final int DEFAULT_BANNER_BACKGROUND_OPACITY = 100;
    public static final int DEFAULT_TIMEOUT_CLOSE = 10000;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 100L;
    private static final long MIN_TIME_BW_UPDATES = 30000L;


    private SlotListener listener;
    private SlotListener listenerApp;
    private String linkAutopromo = "";
    private int autopromoBannerSmall = 0;
    private int autopromoBannerLarge;
    private int autopromoInterstitial = 0;
    private String section = "";

    private boolean isShowing = false;

    private boolean loadAdOnCreate = false;
    private Slot.Init initialize;
    private Activity mActivity;
    private String IDApplication = "YOUR_ID_APPLICATION";
    private DisplayMetrics mMetrics;
    private int format;
    private String currentUrl = "";
    private String networkState = "";
    private String exclude = "";
    private int widthWindow = 0;
    private int heightWindow = 0;

    /**
     * Devices connected location
     */
    private Location location;

    //Banner ID
    private long id_ban = -1;

    //Banner redirection URL
    private String _redirect = "";
    //Banner Content : image url, video url, HTML
    private String contentHTML = "";
    //Banner ContentType: IMAGE, VIDEO, HTML
    private String contentType = "";
    //Banner width
    private int widthPub = 0;
    //Banner height
    private int heightPub = 0;

    private boolean isActive = false;

    private NativeCustomConfiguration nativeCustomConfiguration = null;

    private int bannerRefresh = 0;
    private String bannerBGColor = DEFAULT_BANNER_BACKGROUND_COLOR;
    private int timeout_close = DEFAULT_TIMEOUT_CLOSE;
    private int animationStartDelay = 0;
    private int animationEndDelay = 0;
    private int animationStartID = 0;
    private int animationEndID = 0;
    private int bannerOpacityBG = DEFAULT_BANNER_BACKGROUND_OPACITY;
    private int bannerValign = 0;

    private Animation animationStart = null;
    private Animation animationEnd = null;

    // Final Advertising View
    private View finalView;

    private int buttonCloseWidth = DEFAULT_BUTTON_CLOSE_SIZE;
    private int buttonCloseHeight = DEFAULT_BUTTON_CLOSE_SIZE;
    private int buttonTimeToOnset = DEFAULT_BUTTON_CLOSE_TIME;
    private String buttonCloseUrl = "";
    private boolean buttonCloseVisible = true;

    private boolean forceAutopromo = false;
    private boolean showTimeout = false;

    private int callback_trying = 0;

    private AdsCallback adsCallback = new AdsCallback() {
        public void onShow() {
            Slot.this.show();
        }

        public void onClick() {
            Slot.this.remove();
            if(Slot.this.listener != null) {
                Slot.this.listener.onAdClicked(getFinalView());
            }

        }
    };

    public void callback() {
        if(this.id_ban > 0) {
            this.callback(this.id_ban);
        }

    }

    public void callback(long id_exclude) {
        ++this.callback_trying;
        if(this.callback_trying < 10) {
            if(id_exclude > 0 && !this.exclude.contains(String.valueOf(id_exclude))) {
                this.exclude = this.exclude + (!this.exclude.equals("")?",":"") + "-" + id_exclude;
            }

            (new Slot.Init()).execute();
        } else {
            this.callback_trying = 0;
            this.remove();
        }

    }

    public Slot(Activity _context, String id) {
        super(_context);
        this.mActivity = _context;
        this.IDApplication = id;
        if(this.loadAdOnCreate) {
            this.load(this.format);
        }

    }

    public Slot(Activity _context, int pFormat) {
        super(_context);
        this.mActivity = _context;
        if(!AdsSDK.IDApplication.equals("")) {
            this.IDApplication = AdsSDK.IDApplication;
            this.format = pFormat;
        } else {
            System.out.println("L\'IDApplication n\'est pas renseignee !");
        }

    }

    public Slot(Context _context, AttributeSet attributeSet) {
        super(_context, attributeSet);
        this.mActivity = (Activity)_context;
        if(attributeSet != null) {
            TypedArray a = _context.obtainStyledAttributes(attributeSet, styleable.Slot);
            int N = a.getIndexCount();

            for(int i = 0; i < N; ++i) {
                int attr = a.getIndex(i);
                if(attr == styleable.Slot_format) {
                    this.format = a.getInt(styleable.Slot_format, 0);
                } else if(attr == styleable.Slot_IDApplication) {
                    this.IDApplication = a.getString(styleable.Slot_IDApplication);
                } else if(attr == styleable.Slot_forceAutopromo) {
                    this.forceAutopromo = a.getBoolean(styleable.Slot_forceAutopromo, false);
                } else if(attr == styleable.Slot_autopromoInterstitial) {
                    this.autopromoInterstitial = a.getResourceId(styleable.Slot_autopromoInterstitial, 0);
                } else if(attr == styleable.Slot_autopromoBannerLarge) {
                    this.autopromoBannerLarge = a.getResourceId(styleable.Slot_autopromoBannerLarge, 0);
                } else if(attr == styleable.Slot_autopromoBannerSmall) {
                    this.autopromoBannerSmall = a.getResourceId(styleable.Slot_autopromoBannerSmall, 0);
                } else if(attr == styleable.Slot_linkAutopromo) {
                    this.linkAutopromo = a.getString(styleable.Slot_linkAutopromo);
                } else if(attr == styleable.Slot_showTimeout) {
                    this.showTimeout = a.getBoolean(styleable.Slot_showTimeout, false);
                }
            }

            a.recycle();
        }

        this.load(this.format);
    }

    public Slot(Activity _context, int pFormat, NativeCustomConfiguration pConfiguration) {
        super(_context);
        this.mActivity = _context;
        if(!AdsSDK.IDApplication.equals("")) {
            this.IDApplication = AdsSDK.IDApplication;
            this.format = pFormat;
            this.nativeCustomConfiguration = pConfiguration;
        } else {
            System.out.println("L\'IDApplication n\'est pas renseignee !");
        }

    }

    public void loadRequest() {
        if(this.format > 0) {
            this.load(this.format);
        } else {
            System.out.println("Le format n\'est pas defini");
        }

    }

    public void load(int _format) {
        this.listener = this;
        this.finalView = new View(this.mActivity);
        if(this.format == Format.INTERSTITIAL && AdsSDK.maxInterstitialDelivery > 0 && AdsSDK.getNbInterstitialDelivery() + 1 > AdsSDK.maxInterstitialDelivery) {
            System.out.println("NB max delivered");
        } else {
            if(VERSION.SDK_INT > 9) {
                ThreadPolicy packageManager = (new Builder()).permitAll().build();
                StrictMode.setThreadPolicy(packageManager);
            }

            if(!this.forceAutopromo) {
                PackageManager packageManager1 = this.mActivity.getPackageManager();
                if(packageManager1 != null && (packageManager1.checkPermission("android.permission.ACCESS_FINE_LOCATION", this.mActivity.getPackageName()) == 0 || packageManager1.checkPermission("android.permission.ACCESS_COARSE_LOCATION", this.mActivity.getPackageName()) == 0)) {
                    try {
                        LocationManager locationManager = (LocationManager)this.mActivity.getSystemService(Context.LOCATION_SERVICE);
                        boolean isGPSEnabled = locationManager.isProviderEnabled("gps");
                        boolean isNetworkEnabled = locationManager.isProviderEnabled("network");
                        if(isGPSEnabled || isNetworkEnabled) {
                            LocationListener locationListener = new LocationListener() {
                                public void onLocationChanged(Location location) {
                                }

                                public void onProviderDisabled(String provider) {
                                }

                                public void onProviderEnabled(String provider) {
                                }

                                public void onStatusChanged(String provider, int status, Bundle extras) {
                                }
                            };
                            if(isGPSEnabled) {
                                locationManager.requestLocationUpdates("gps", 30000L, 100.0F, locationListener);
                                if(locationManager != null) {
                                    this.location = locationManager.getLastKnownLocation("gps");
                                }
                            }

                            if(isNetworkEnabled) {
                                locationManager.requestLocationUpdates("network", 30000L, 100.0F, locationListener);
                                if(locationManager != null) {
                                    this.location = locationManager.getLastKnownLocation("network");
                                }
                            }
                        }
                    } catch (Exception var7) {
                        ;
                    }
                }
            } else {
                this.linkAutopromo = Globals.updateMacro(this.linkAutopromo);
            }

            this.format = _format;
            this.mMetrics = this.mActivity.getResources().getDisplayMetrics();
            this.widthWindow = this.mMetrics.widthPixels;
            this.heightWindow = this.mMetrics.heightPixels;
            this.initialize = new Slot.Init();
            this.initialize.execute();
        }
    }

    private void configure(JSONObject _json) {
        if(_json == null) {
            System.out.println("Erreur fichier JSON");
        } else {
            JSONObject customize = null;
            JSONObject config = null;

            try {
                config = _json.getJSONObject(CONFIG);

                try {
                    this.id_ban = config.getLong(CONFIG_ID_BAN);
                } catch (Exception var14) {
                    ;
                }


                try {
                    this._redirect = config.getString(CONFIG_REDIRECT).toString();
                    this._redirect = Globals.updateMacro(this._redirect);
                } catch (Exception var11) {
                    ;
                }

                try {
                    this.contentHTML = config.getString(CONFIG_CONTENT).toString();
                } catch (Exception var9) {
                    ;
                }

                try {
                    this.contentType = config.getString(CONFIG_TYPE).toString();
                } catch (Exception var8) {
                    ;
                }

                try {
                    this.widthPub = config.getJSONObject(CONFIG_SIZE).getInt(CONFIG_SIZE_WIDTH);
                } catch (Exception var7) {
                    ;
                }

                try {
                    this.heightPub = config.getJSONObject(CONFIG_SIZE).getInt(CONFIG_SIZE_HEIGHT);
                } catch (Exception var6) {
                    ;
                }

                try {
                    customize = _json.getJSONObject(CUSTOMIZE);
                } catch (JSONException var5) {
                  ;
                }

                this.customize(customize);
                if(this.forceAutopromo) {
                    this.callAutopromo();
                } else {
                    if(this.id_ban > 0 && this.format == Format.INTERSTITIAL && this.contentType.equals(JSON.ContentType.VIDEO) && !this.networkState.equals("WIFI") && !this.networkState.equals("4G") && !this.networkState.equals("3G")) {
                        this.callback(this.id_ban);
                        return;
                    }

                    if(this.id_ban > 0 && this.format == Format.INTERSTITIAL) {
                        this.show();
                        return;
                    }
                }
            } catch (JSONException var15) {
                ;
            }

            if(this.format != Format.INTERSTITIAL) {
                if(this.contentType.toUpperCase().equals(JSON.ContentType.HTML)) {
                    this.finalView = Constructor.createWebView(this.getActivityContext(), this.contentHTML, this.bannerBGColor, true, this.adsCallback);
                } else if(this.contentType.toUpperCase().equals(JSON.ContentType.IMAGE)) {
                    this.finalView = Constructor.createImageView(this.getActivityContext(), this.contentHTML, this._redirect, true, this.getPubWidth(), this.getPubHeight(), this.adsCallback);
                } else if(this.contentType.toUpperCase().equals(JSON.ContentType.VIDEO)) {
                    this.finalView = Constructor.createVideoView(this.getActivityContext(), this.contentHTML, this._redirect, true, this.adsCallback);
                }

                this.finalView.setBackgroundColor(0);
            }
        }

    }

    private void customize(JSONObject customize) {
        if(customize != null) {
            try {
                String bannerValignName = customize.getString(CUSTOMIZE_VALIGN);
                if(bannerValignName.toUpperCase().equals(JSON.Valign.TOP)) {
                    this.bannerValign = 10;
                } else if(bannerValignName.toUpperCase().equals(JSON.Valign.BOTTOM)) {
                    this.bannerValign = 12;
                }

                if(!customize.getString(CUSTOMIZE_TIMEOUT_CLOSE).equals("")) {
                    this.timeout_close = customize.getInt(CUSTOMIZE_TIMEOUT_CLOSE);
                } else {
                    this.timeout_close = 0;
                }

                this.animationStartDelay = Animations.getAnimationSpeed(customize.getJSONObject(CUSTOMIZE_ANIMATION).getJSONObject(CUSTOMIZE_ANIMATION_START).getString( CUSTOMIZE_ANIMATION_START_DELAY));
                this.animationEndDelay = Animations.getAnimationSpeed(customize.getJSONObject(CUSTOMIZE_ANIMATION).getJSONObject(CUSTOMIZE_ANIMATION_END).getString(CUSTOMIZE_ANIMATION_END_DELAY ));
                this.animationStartID = Animations.getIDAnimation(customize.getJSONObject(CUSTOMIZE_ANIMATION).getJSONObject(CUSTOMIZE_ANIMATION_START).getString(CUSTOMIZE_ANIMATION_START_EFFECT));
                this.animationEndID = Animations.getIDAnimation(customize.getJSONObject(CUSTOMIZE_ANIMATION).getJSONObject(CUSTOMIZE_ANIMATION_END).getString(CUSTOMIZE_ANIMATION_END_EFFECT));
                this.animationStart = Animations.Animate(this.animationStartID, this.animationStartDelay);
                this.animationEnd = Animations.Animate(this.animationEndID, this.animationEndDelay);
                this.bannerBGColor = customize.getString(CUSTOMIZE_BACKGROUND_COLOR);
                if(!customize.getString(CUSTOMIZE_BACKGROUND_OPACITY).equals("")) {
                    this.bannerOpacityBG = customize.getInt(CUSTOMIZE_BACKGROUND_OPACITY);
                } else {
                    this.bannerOpacityBG = 100;
                }

                this.bannerRefresh = 0;
                if(this.format == Format.BANNER && !customize.getString(CUSTOMIZE_REFRESH).equals("")) {
                    this.bannerRefresh = customize.getInt(CUSTOMIZE_REFRESH);
                }

                if(!customize.getJSONObject(CUSTOMIZE_BUTTON_CLOSE).getJSONObject(CUSTOMIZE_BUTTON_CLOSE_SIZE).getString(CUSTOMIZE_BUTTON_CLOSE_SIZE_WIDTH).equals("")) {
                    this.buttonCloseWidth = customize.getJSONObject(CUSTOMIZE_BUTTON_CLOSE).getJSONObject(CUSTOMIZE_BUTTON_CLOSE_SIZE).getInt(CUSTOMIZE_BUTTON_CLOSE_SIZE_WIDTH);
                }

                if(!customize.getJSONObject(CUSTOMIZE_BUTTON_CLOSE).getJSONObject(CUSTOMIZE_BUTTON_CLOSE_SIZE).getString(CUSTOMIZE_BUTTON_CLOSE_SIZE_HEIGHT).equals("")) {
                    this.buttonCloseHeight = customize.getJSONObject(CUSTOMIZE_BUTTON_CLOSE).getJSONObject(CUSTOMIZE_BUTTON_CLOSE_SIZE).getInt(CUSTOMIZE_BUTTON_CLOSE_SIZE_HEIGHT);
                }

                if(!customize.getJSONObject(CUSTOMIZE_BUTTON_CLOSE).getString(CUSTOMIZE_BUTTON_CLOSE_TIME_TO_ONSET).equals("")) {
                    this.buttonTimeToOnset = customize.getJSONObject(CUSTOMIZE_BUTTON_CLOSE).getInt(CUSTOMIZE_BUTTON_CLOSE_TIME_TO_ONSET);
                }

                if(!customize.getJSONObject(CUSTOMIZE_BUTTON_CLOSE).getString(CUSTOMIZE_BUTTON_CLOSE_URL_BACKGROUND).equals("")) {
                    this.buttonCloseUrl = customize.getJSONObject(CUSTOMIZE_BUTTON_CLOSE).getString(CUSTOMIZE_BUTTON_CLOSE_URL_BACKGROUND);
                }

                this.buttonCloseVisible = true;
            } catch (JSONException var3) {
                ;
            }
        }

    }

    public void show() {
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if(!Slot.this.isShowing && (Slot.this.forceAutopromo || Slot.this.id_ban > 0) && Slot.this.format == Format.INTERSTITIAL) {
                    Slot.this.isShowing = true;
                    Intent intent1 = new Intent(Slot.this.mActivity, AdsActivity.class);
                    intent1.putExtra("buttonCloseUrl", Slot.this.buttonCloseUrl);
                    intent1.putExtra("buttonTimeToOnset", Slot.this.buttonTimeToOnset);
                    intent1.putExtra("timeout_close", Slot.this.timeout_close);
                    intent1.putExtra("contentType", Slot.this.contentType);
                    intent1.putExtra("bannerBGColor", Slot.this.bannerBGColor);
                    intent1.putExtra("redirect", Slot.this._redirect);
                    intent1.putExtra("showTimeout", Slot.this.showTimeout);
                    intent1.putExtra("animationStartID", Slot.this.animationStartID);
                    if(Slot.this.finalView.getTag() != null && !Slot.this.finalView.getTag().toString().equals("")) {
                        intent1.putExtra("contentHTML", Slot.this.finalView.getTag().toString());
                    } else {
                        intent1.putExtra("contentHTML", Slot.this.contentHTML.toString());
                    }

                    if(Slot.this.forceAutopromo) {
                        intent1.putExtra("linkAutopromo", Slot.this.linkAutopromo);
                        intent1.putExtra("autopromoInterstitial", Slot.this.autopromoInterstitial);
                        intent1.putExtra("forceAutopromo", Slot.this.forceAutopromo);
                    }


                    if(Slot.this.listener != null) {
                        Slot.this.listener.onAdShown(Slot.this.finalView);
                    }

                    Slot.this.mActivity.startActivity(intent1);
                } else {
                    if(!Slot.this.isShowing && (Slot.this.forceAutopromo || Slot.this.id_ban > 0) && Slot.this.format == Format.BANNER ) {
                        Slot.this.isShowing = true;
                        if(Slot.this.bannerBGColor != null && !Slot.this.bannerBGColor.equals("")  && Slot.this.bannerOpacityBG > 0 && Slot.this.bannerOpacityBG < 100) {
                            Slot.this.getBackground().setAlpha((int)(255.0D * ((double)((float)Slot.this.bannerOpacityBG) / 100.0D)));
                        }

                        Slot.this.addToLayout();
                        if(Slot.this.animationStart != null) {
                            Slot.this.finalView.startAnimation(Slot.this.animationStart);
                        }

                        if(Slot.this.listener != null) {
                            Slot.this.listener.onAdShown(Slot.this.finalView);
                        }
                    }

                }
            }
        });
    }

    public void addToLayout() {
        if(this.finalView != null) {
            LayoutParams params = new LayoutParams(-1, -2);
            this.setOrientation(VERTICAL);
            this.setLayoutParams(params);
            int widthPubDip = this.widthPub;
            int heightPubDip = this.heightPub;
            if(!this.forceAutopromo) {
                widthPubDip = (int)((float)this.widthPub * this.mMetrics.density);
                heightPubDip = (int)((float)this.heightPub * this.mMetrics.density);
            }


            params = new LayoutParams(widthPubDip, heightPubDip);


            params.gravity = Gravity.CENTER_HORIZONTAL;
            this.finalView.setLayoutParams(params);
            this.finalView.setVisibility(VISIBLE);
            this.removeAllViews();
            this.addView(this.finalView, params);
            this.setVisibility(VISIBLE);
        }

    }

    private void refresh() {
        if(this.bannerRefresh > 0) {
            this.postDelayed(new Runnable() {
                public void run() {
                    Slot.this.isActive = true;
                    (Slot.this.new Init()).execute();
                    Slot.this.listener.onAdRefreshed(Slot.this.finalView);
                }
            }, (long)(this.bannerRefresh * 1000));
        }

    }

    private void callAutopromo() {
        this.forceAutopromo = true;
        boolean resourceImage = false;
        int resourceImage1;
        if(this.format == Format.BANNER) {
            if(this.widthWindow < this.heightWindow) {
                resourceImage1 = this.autopromoBannerSmall;
            } else {
                resourceImage1 = this.autopromoBannerLarge;
            }
        } else {
            resourceImage1 = this.autopromoInterstitial;
        }

        if(resourceImage1 > 0) {
            Drawable d = this.mActivity.getResources().getDrawable(resourceImage1);
            if(this.format == Format.BANNER) {
                this.widthPub = d.getIntrinsicWidth();
                this.heightPub = d.getIntrinsicHeight();
            }

            this.finalView = Constructor.createImageView(this.getActivityContext(), resourceImage1, this.linkAutopromo, false, this.adsCallback);
            this.show();
        }

    }

    private void close() {
        this.listenerApp = null;
        this.listener = null;
        if(this.mMetrics != null) {
            this.mMetrics = null;
        }

        if(this.finalView != null) {
            this.finalView = null;
        }

        if(this.initialize != null) {
            this.initialize = null;
        }

        this.animationEnd = null;
        this.animationStart = null;
        if(this.location != null) {
            this.location = null;
        }

        this.remove();
    }

    public void remove() {
        // TODO nothing
    }

    public void addInterstitialDelivery() {
        AdsSDK.addNbInterstitialDelivery();
    }

    public void onAdLoadingFailed(View view) {
        System.out.println("onAdLoadingFailed");
        if(this.listenerApp != null) {
            this.listenerApp.onAdLoadingFailed(view);
        }

    }

    public void onAdLoaded(View view) {
        if(this.listenerApp != null) {
            this.listenerApp.onAdLoaded(view);
        }

    }

    public void onAdClicked(View view) {
        if(this.id_ban > 0 && this.location != null) {
            Globals.trackEvent(getActivity(),this.id_ban, AdsSDK.AdsEvent.CLICKED, this.location);
        }

        if(this.listenerApp != null) {
            this.listenerApp.onAdClicked(view);
        }

    }

    public void onAdRefreshed(View view) {
        if(this.listenerApp != null) {
            this.listenerApp.onAdRefreshed(view);
        }

    }

    public void onAdClosed(View view) {
        if(this.listenerApp != null) {
            this.listenerApp.onAdClosed(view);
        }

        this.close();
    }

    public void onAdShown(View view) {
        this.isShowing = false;
        if(this.format == Format.INTERSTITIAL) {
            this.addInterstitialDelivery();
        }

        if(this.id_ban > 0 && this.location != null) {
            Globals.trackEvent(getActivity(),this.id_ban, AdsSDK.AdsEvent.SHOWN, this.location);
        }

        if(this.listenerApp != null) {
            this.listenerApp.onAdShown(view);
        }

    }

    public int getFormat() {
        return this.format;
    }

    public int getPubWidth() {
        return this.widthPub;
    }

    public int getPubHeight() {
        return this.heightPub;
    }

    public int getTimeRefresh() {
        return this.bannerRefresh;
    }

    public String getIDApplication() {
        return this.IDApplication;
    }

    public DisplayMetrics getMetrics() {
        return this.mMetrics;
    }

    public NativeCustomConfiguration getNativeCustomConfiguration() {
        return this.nativeCustomConfiguration;
    }

    public Activity getActivity() {
        return this.mActivity;
    }

    protected Serializable getSlot() {
        return this;
    }

    public View getFinalView() {
        return this.finalView;
    }

    public SlotListener getListener() {
        return (SlotListener)(this.listener != null?this.listener:this);
    }

    public SlotListener getListenerApp() {
        return (SlotListener)(this.listenerApp != null?this.listenerApp:this);
    }

    public Context getActivityContext() {
        return this.mActivity != null?this.mActivity.getApplicationContext():null;
    }

    public String getLinkAutopromo() {
        return this.linkAutopromo;
    }

    public int getAutopromoBannerSmall() {
        return this.autopromoBannerSmall;
    }

    public int getAutopromoBannerLarge() {
        return this.autopromoBannerLarge;
    }

    public int getAutopromoInterstitial() {
        return this.autopromoInterstitial;
    }

    public String getSection() {
        return this.section;
    }

    public void setWidth(int width) {
        this.widthPub = width;
    }

    public void setHeight(int height) {
        this.heightPub = height;
    }

    public void setContentType(String type) {
        this.contentType = type;
    }

    public void setSlotListener(Slot pListener) {
        this.listenerApp = pListener;
    }

    public void setSlotListener(SlotListener pListener) {
        this.listenerApp = pListener;
    }

    public void setIDApplication(String IDApplication) {
        this.IDApplication = IDApplication;
    }

    public void setMetrics(DisplayMetrics mMetrics) {
        this.mMetrics = mMetrics;
    }

    public void setForceAutopromo(boolean forceAutopromo) {
        this.forceAutopromo = forceAutopromo;
    }

    public void setLinkAutopromo(String linkAutopromo) {
        this.linkAutopromo = linkAutopromo;
    }

    public void setAutopromoBannerSmall(int autopromoBannerSmall) {
        this.autopromoBannerSmall = autopromoBannerSmall;
    }

    public void setAutopromoBannerLarge(int autopromoBannerLarge) {
        this.autopromoBannerLarge = autopromoBannerLarge;
    }

    public void setAutopromoInterstitial(int autopromoInterstitial) {
        this.autopromoInterstitial = autopromoInterstitial;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setNativeCustomConfiguration(NativeCustomConfiguration nativeCustomConfiguration) {
        this.nativeCustomConfiguration = nativeCustomConfiguration;
    }

    public void setFinalView(View v) {
        this.finalView = v;
    }

    public void setActivity(Activity _activity) {
        this.mActivity = _activity;
    }

    private class Init extends AsyncTask<Void, Void, String> {
        JSONObject json;
        Boolean callbackAutopromo;

        private Init() {
            this.json = null;
            this.callbackAutopromo = Boolean.valueOf(false);
        }

        protected String doInBackground(Void... params) {
            if(!Slot.this.IDApplication.equals("YOUR_ID_APPLICATION") && !Slot.this.IDApplication.equals("")) {

                if(!Slot.this.forceAutopromo) {
                    if(Globals.isConnectedNetwork(Slot.this.mActivity)) {

                        Slot.this.currentUrl = AdsSDK.BASE_API_URL;
                        Slot.this.currentUrl = Slot.this.currentUrl + (!Slot.this.exclude.equals("")?"&excludes=" + Slot.this.exclude:"");
                        Slot.this.currentUrl = Slot.this.currentUrl + "&idapp=" + Integer.parseInt(Slot.this.IDApplication);
                        Slot.this.currentUrl = Slot.this.currentUrl + "&format=" + Slot.this.format;
                        Slot.this.currentUrl = Slot.this.currentUrl + "&w=" + Slot.this.widthWindow + "&h=" + Slot.this.heightWindow;

                        Slot.this.currentUrl = Slot.this.currentUrl + "&isMobile=1";

                        Slot.this.currentUrl = Slot.this.currentUrl + "&ms="+System.currentTimeMillis();

                        if(Slot.this.location != null) {
                            Slot.this.currentUrl = Slot.this.currentUrl + "&geocoord=" + Slot.this.location.getLatitude() + ";" + Slot.this.location.getLongitude();
                        }

                        Slot.this.currentUrl = Slot.this.currentUrl + "&isTesting="+(AdsSDK.testing?"1":"0");

                        Slot.this.networkState = Globals.getStateNetwork(Slot.this.mActivity);

                        Slot.this.currentUrl = Slot.this.currentUrl + "&network=" + Slot.this.networkState ;

                        Slot.this.currentUrl = Slot.this.currentUrl + "&deviceId=" + Globals.getDeviceId(Slot.this.mActivity);

                        Slot.this.currentUrl = Slot.this.currentUrl + "&devicesOSVersion=Android " + Globals.getOSVersion();

                        Slot.this.currentUrl = Slot.this.currentUrl + "&devicesName=" + Globals.getDeviceName();


                        String jsonString = Get.getHtml(Slot.this.currentUrl);

                        try {
                            if(jsonString!= null && jsonString.contains("\"CONFIG\":")) {
                                this.json = new JSONObject(jsonString );
                                Slot.this.mActivity.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Slot.this.refresh();
                                    }
                                });
                            }
                        } catch (JSONException var5) {
                            ;
                        }
                    } else {
                        this.callbackAutopromo = Boolean.valueOf(true);
                    }
                } else {
                    this.callbackAutopromo = Boolean.valueOf(true);
                }
            } else {
                this.callbackAutopromo = Boolean.valueOf(true);
            }

            if(this.callbackAutopromo.booleanValue()) {
                Slot.this.mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        Slot.this.callAutopromo();
                    }
                });
            }

            return null;
        }

        protected void onPostExecute(String result) {
            if(this.json != null) {
                Slot.this.configure(this.json);
            }

        }

        protected void onPreExecute() {
        }

        protected void onProgressUpdate(Void... values) {
            System.out.println("process update");
        }
    }
}
