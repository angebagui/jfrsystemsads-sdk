package ci.jfrsystems.opensdk.listeners;

interface SlotListenerHandler {
    void onHandle(Object var1);
}
