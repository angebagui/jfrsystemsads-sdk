package ci.jfrsystems.opensdk.listeners;

import android.view.View;


public interface SlotListener {
    void onAdLoadingFailed(View var1);

    void onAdLoaded(View var1);

    void onAdClicked(View var1);

    void onAdRefreshed(View var1);

    void onAdClosed(View var1);

    void onAdShown(View var1);
}
