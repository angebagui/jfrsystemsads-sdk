package ci.jfrsystems.opensdk;

/**
 * Created by angebagui on 24/06/2017.
 */

public final class AdsSDK {

    public static final String BASE_API_URL = "https://jfrsystemsads.ci/richmedia.ads?";
    public static final String API_TRACK_EVENT_URL = "https://jfrsystemsads.ci/track.ads?";
    //Example
    /* GET
      https://jfrsystemsads.ci/richmedia.ads?
      &excludes=424,12
      &idapp=2232222
      &format=14 or 15
      &w=800
      &h=800
      &ms= timestamp
      &isMobile=1
      &isTesting=1 or 0
      &network=WIFI or GPRS or 3G or 4G or Unknown
      &geocoord=4.322112222;-5.62232222,
      &deviceId=24333
      &devicesOSVersion=Android%207
      &devicesName=TECHNO%20CAMON%20CX%20Air
     */



    //Example
    /* GET
     * https://jfrsystemsads.ci/track.ads?
     * idbanner=12333
     * &idapp=2232222
     * &event=1002 or 1001
     * &ms= timestamp
     * &isMobile=1
      &isTesting=1 or 0
      &network=WIFI or GPRS or 3G or 4G or Unknown
      &geocoord=4.322112222;-5.62232222,
      &deviceId=24333
      &devicesOSVersion=Android%207
      &devicesName=TECHNO%20CAMON%20CX%20Air

     int EVENT_CLICKED = 1002;
     int EVENT_SHOWN = 1001;
     int FORMAT_INTERSTITIAL = 14;
     int FORMAT_BANNER = 15;

     */

    public static String IDApplication = "";
    public static int maxInterstitialDelivery = 0;
    public static boolean testing = false;

    private static int nbInterstitialDelivery = 0;

    public AdsSDK() {
    }

    public static String getIDApplication() {
        return IDApplication;
    }

    public static void setIDApplication(String IDApplication) {
        AdsSDK.IDApplication = IDApplication;
    }

    public static void addNbInterstitialDelivery() {
        ++nbInterstitialDelivery;
    }

    public static int getNbInterstitialDelivery() {
        return nbInterstitialDelivery;
    }

    public static class AdsEvent{
        public static final int CLICKED = 1002;
        public static final int SHOWN = 1001;
    }
}
