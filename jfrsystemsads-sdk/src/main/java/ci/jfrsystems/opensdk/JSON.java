package ci.jfrsystems.opensdk;

/**
 * Created by angebagui on 24/06/2017.
 */

public class JSON {

    public static class Key{

        public static final  String CONFIG = "CONFIG";
        public static final  String CONFIG_ID_BAN = "ID_BAN";
        public static final  String CONFIG_IDL = "IDL";
       // public static final  String CONFIG_SDK_CALLBACK = "SDK_CALLBACK";
        public static final  String CONFIG_REDIRECT = "REDIRECT";
        public static final  String CONFIG_TRACKPAP = "TRACKPAP";
        public static final  String CONFIG_CONTENT = "CONTENT";
        public static final  String CONFIG_TYPE = "TYPE";
        public static final  String CONFIG_SIZE = "SIZE";
        public static final  String CONFIG_SIZE_WIDTH = "WIDTH";
        public static final  String CONFIG_SIZE_HEIGHT = "HEIGHT";

        public static final  String CUSTOMIZE = "CUSTOMIZE";
        public static final  String CUSTOMIZE_VALIGN = "VALIGN";
        public static final  String CUSTOMIZE_TIMEOUT_CLOSE = "TIMEOUT_CLOSE";

        public static final  String CUSTOMIZE_ANIMATION = "ANIMATION";

        public static final  String CUSTOMIZE_ANIMATION_START = "START";
        public static final  String CUSTOMIZE_ANIMATION_START_DELAY = "DELAY";
        public static final  String CUSTOMIZE_ANIMATION_START_EFFECT = "EFFECT";

        public static final  String CUSTOMIZE_ANIMATION_END = "END";
        public static final  String CUSTOMIZE_ANIMATION_END_DELAY = "DELAY";
        public static final  String CUSTOMIZE_ANIMATION_END_EFFECT = "EFFECT";


        public static final  String CUSTOMIZE_BACKGROUND_COLOR = "BACKGROUND_COLOR";
        public static final  String CUSTOMIZE_BACKGROUND_OPACITY = "BACKGROUND_OPACITY";
        public static final  String CUSTOMIZE_REFRESH= "REFRESH";

        public static final  String CUSTOMIZE_BUTTON_CLOSE = "BUTTON_CLOSE";
        public static final  String CUSTOMIZE_BUTTON_CLOSE_SIZE = "SIZE";
        public static final  String CUSTOMIZE_BUTTON_CLOSE_SIZE_WIDTH = "WIDTH";
        public static final  String CUSTOMIZE_BUTTON_CLOSE_SIZE_HEIGHT= "HEIGHT";
        public static final  String CUSTOMIZE_BUTTON_CLOSE_TIME_TO_ONSET = "TIME_TO_ONSET";
        public static final  String CUSTOMIZE_BUTTON_CLOSE_URL_BACKGROUND = "URL";
    }

    public static class ContentType{
        public static final String VIDEO = "VIDEO";
        public static final String IMAGE = "IMAGE";
        public static final String HTML = "HTML";
    }

    public static class Valign{
        public static final String TOP = "TOP";
        public static final String BOTTOM = "BOTTOM";
     }
}
