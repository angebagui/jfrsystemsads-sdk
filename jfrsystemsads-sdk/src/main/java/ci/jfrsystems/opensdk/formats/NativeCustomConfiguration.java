package ci.jfrsystems.opensdk.formats;

public class NativeCustomConfiguration implements NativeCustomConfigurationInterface {
    public NativeCustomConfiguration() {
    }

    public NativeCustomConfiguration setBackgroundColor(int color) {
        try {
            config.put("backgroundColor", color);
        } catch (Exception var3) {
            ;
        }

        return this;
    }

    public NativeCustomConfiguration setTitleColor(int color) {
        try {
            config.put("titleColor", color);
        } catch (Exception var3) {
            ;
        }

        return this;
    }

    public NativeCustomConfiguration setButtonColor(int color) {
        try {
            config.put("buttonColor", color);
        } catch (Exception var3) {
            ;
        }

        return this;
    }

    public int getBackgroundColor() {
        int c = 0;

        try {
            c = config.getInt("backgroundColor");
        } catch (Exception var3) {
            ;
        }

        return c;
    }

    public int getTitleColor() {
        int c = 0;

        try {
            c = config.getInt("titleColor");
        } catch (Exception var3) {
            ;
        }

        return c;
    }

    public int getButtonColor() {
        int c = 0;

        try {
            c = config.getInt("buttonColor");
        } catch (Exception var3) {
            ;
        }

        return c;
    }
}