package ci.jfrsystems.opensdk.formats;

import org.json.JSONObject;

interface NativeCustomConfigurationInterface {
    JSONObject config = new JSONObject();

    NativeCustomConfiguration setBackgroundColor(int var1);

    NativeCustomConfiguration setTitleColor(int var1);

    NativeCustomConfiguration setButtonColor(int var1);

    int getBackgroundColor();

    int getTitleColor();

    int getButtonColor();
}