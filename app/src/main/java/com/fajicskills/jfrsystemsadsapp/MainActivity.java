package com.fajicskills.jfrsystemsadsapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import ci.jfrsystems.opensdk.*;
import ci.jfrsystems.opensdk.formats.*;
import ci.jfrsystems.opensdk.listeners.*;
import ci.jfrsystems.opensdk.views.*;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // MY_LAYOUT is the ID of the Linear Layout which whill be holding the ad
        final LinearLayout ll = (LinearLayout) findViewById(R.id.bannerLayout);

        Slot bannerAd = new Slot(this, Format.BANNER);

        bannerAd.setSlotListener(new SlotListener() {
            @Override
            public void onAdLoadingFailed(View view) {
                // ad loading failed (eg. no ad, network error, etc.)
            }

            @Override
            public void onAdLoaded(View view) {
                // ad loaded
            }

            @Override
            public void onAdClicked(View view) {
                // ad clicked
            }

            @Override
            public void onAdRefreshed(View view) {
                // ad refreshed (unused)
            }

            @Override
            public void onAdClosed(View view) {
                // ad closed (interstitials)
            }

            @Override
            public void onAdShown(View view) {
                // ad shown
            }
        });

        bannerAd.loadRequest();

        ll.addView(bannerAd);

        Log.e(MainActivity.class.getSimpleName(), "Format >>> "+Format.BANNER);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
