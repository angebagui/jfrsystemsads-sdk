package com.fajicskills.jfrsystemsadsapp;

import android.app.Application;

import ci.jfrsystems.opensdk.AdsSDK;

/**
 * Created by angebagui on 25/06/2017.
 */

public class MyApp extends Application {

    private  static final String ID_APPLICATION = "MON_APPLICATION_ID";

    @Override
    public void onCreate() {
        super.onCreate();
        AdsSDK.setIDApplication(ID_APPLICATION);
    }
}
